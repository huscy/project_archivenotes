from rest_framework.routers import DefaultRouter

from huscy.project_archivenotes import views


router = DefaultRouter()
router.register('archivenotes', views.ArchiveNoteViewSet)
