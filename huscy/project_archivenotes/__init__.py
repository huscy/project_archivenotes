# major.minor.patch.release.number
# release must be one of alpha, beta, rc or final
VERSION = (0, 1, 0, 'alpha', 0)

__version__ = '.'.join(str(x) for x in VERSION)
